package by.bsu.ratkevich.mylibrary;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListShufflerFragment extends Fragment {

    private ListView listView;

    public ListShufflerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final List<String> shuffleList = new ArrayList<>();
        final View view = inflater.inflate(R.layout.fragment_list_shuffler, container, false);
        listView = (ListView) view.findViewById(R.id.shuffleList);
        final Context context = inflater.getContext();
        listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, shuffleList));

        final EditText shuffleTextView = (EditText) view.findViewById(R.id.shuffleTextView);
        final Button shuffleBtn = (Button) view.findViewById(R.id.shuffleBtn);
        final Button clearBtn = (Button) view.findViewById(R.id.clearBtn);

        shuffleTextView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    shuffleList.add(shuffleTextView.getText().toString());
                    shuffleTextView.setText("");
                    if (shuffleTextView.requestFocus()) {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                    return true;
                }
                return false;
            }
        });

        shuffleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.shuffle(shuffleList);
                listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, shuffleList));
                MainActivity.setResult(shuffleList.toString());
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shuffleList.clear();
                listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, shuffleList));
            }
        });

        return view;
    }

}
