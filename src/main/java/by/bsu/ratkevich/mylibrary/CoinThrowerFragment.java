package by.bsu.ratkevich.mylibrary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class CoinThrowerFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_coin_thrower, container, false);
        final ImageView coinImageView = (ImageView) view.findViewById(R.id.coinImageView);

        coinImageView.setClickable(true);
        final Random rnd = new Random();
        coinImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int choice = rnd.nextInt(2);
                String result;
                if (choice == 0) {
                    result = "HEADS!";
                    coinImageView.setImageResource(R.drawable.coin_1);
                } else {
                    result = "TAILS!";
                    coinImageView.setImageResource(R.drawable.coin_2);
                }

                Toast.makeText(view.getContext(), result, Toast.LENGTH_LONG).show();
                MainActivity.setResult(result);
            }
        });
        return view;
    }

}
