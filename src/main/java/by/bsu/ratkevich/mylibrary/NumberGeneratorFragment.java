package by.bsu.ratkevich.mylibrary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class NumberGeneratorFragment extends Fragment {


    public NumberGeneratorFragment() {
        // Required empty public constructor
    }

    private static final int BOUND = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_number_generator, container, false);

        final TextView generateTextField = (TextView) view.findViewById(R.id.generateNumberTextView);

        Button generateBtn = (Button) view.findViewById(R.id.generateNumberBtn);
        generateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = String.valueOf(new Random().nextInt(BOUND));
                generateTextField.setText(result);
                MainActivity.setResult(result);
            }
        });

        String result = String.valueOf(new Random().nextInt(BOUND));
        generateTextField.setText(result);
        MainActivity.setResult(result);

        return view;
    }

}
