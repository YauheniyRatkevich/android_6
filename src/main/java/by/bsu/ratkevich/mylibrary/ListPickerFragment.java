package by.bsu.ratkevich.mylibrary;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListPickerFragment extends Fragment {

    private ListView listView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final List<String> pickList = new ArrayList<>();
        final View view = inflater.inflate(R.layout.fragment_list_picker, container, false);
        listView = (ListView) view.findViewById(R.id.pickList);
        final Context context = inflater.getContext();
        listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, pickList));

        final EditText pickTextView = (EditText) view.findViewById(R.id.pickTextView);
        final Button pickBtn = (Button) view.findViewById(R.id.pickBtn);
        final Button clearBtn = (Button) view.findViewById(R.id.clearBtn);

        pickTextView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    pickList.add(pickTextView.getText().toString());
                    pickTextView.setText("");
                    if (pickTextView.requestFocus()) {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                    return true;
                }
                return false;
            }
        });

        pickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = pickList.size();
                if (size > 0) {
                    String picked = pickList.get(new Random().nextInt(size)) + " was picked.";
                    MainActivity.setResult(picked);
                    Snackbar.make(view, picked,
                            Snackbar.LENGTH_INDEFINITE).show();

                } else {
                    Snackbar.make(view, "Nothing to pick.", Snackbar.LENGTH_INDEFINITE).show();
                }


            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickList.clear();
                listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, pickList));
            }
        });

        return view;
    }

}
